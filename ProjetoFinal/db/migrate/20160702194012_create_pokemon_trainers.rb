class CreatePokemonTrainers < ActiveRecord::Migration
  def change
    create_table :pokemon_trainers do |t|
      t.string :name
      t.integer :age
      t.string :sex
      t.string :city

      t.timestamps null: false
    end
  end
end
