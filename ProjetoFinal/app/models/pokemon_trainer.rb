class PokemonTrainer < ActiveRecord::Base

	validates_presence_of :name, :age, :sex, :city,
	:message => "não pode estar em branco."
	validates_numericality_of :age, :message => "não é um número."

end
