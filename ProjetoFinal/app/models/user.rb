class User < ActiveRecord::Base
	enum role: [:usuario_comum,:admin]

	has_one :pokemon_trainer
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
