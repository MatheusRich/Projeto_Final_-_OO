TEMA E OBJETIVOS
================

Neste projeto tivemos como objetivo criar um website em Ruby utilizando o framework Rails. Este website deveria ter:

- Aplicação Web com uma página inicial pública apresentando o serviço.
- Área restrita para usuários cadastrados (Gerenciar o cadastro dos usuários)
- Perfil de usuários (Padrão e Administrador) - Níveis de permissão
- A aplicação deverá conter pelo menos 1 item CRUD além do cadastro dos usuários.

Com base nestas especificações foi escolhido um "Cadastro de Treinadores Pokémon" como tema para o projeto.
Este cadastro seria para incluir jogadores a um jogo fictício, possibilitando um ambiente online.

FUNCIONALIDADES DA APLICAÇÃO
============================

- Tela inicial com informações sobre o jogo(fictício) e links para diversas áreas, tanto internas quanto externas.
- Página explicando o que é o jogo(fictício).
- Cadastro de usuários do site, com a possibilidade de alteração de login e/ou senha no futuro.
- Criação, alteração e destruição de Treinadores Pokémon(avatares) pelo usuário cadastrado. Cada usuário pode ter inúmeros avatares.
- Personalização de cada avatar pelo usuário.
- Gerenciamento de usuários pelo Usuário Administrador.



MODELS E MÉTODOS
================

POKEMON_TRAINER
---------------

- Esta é a model base do site, onde o usuário criará seu treinador pokémon, podendo editá-lo ou mesmo excluí-lo quando quiser.

**MÉTODOS**

-*NEW:* Cria um novo treinador pokémon.
-*SHOW:* Mostra os dados do treinador pokémon. 
-*EDIT:* Edita os dados de um treinador pokémon existente.
-*SHOW:* Mostra os dados do treinador pokémon.
-*CREATE:* Cria um treinador pokémon de acordo com o formato especificado.
-*UPDATE:* Atualiza os dados de um treinador pokémon.
-*DESTROY:* Apaga um treinador pokémon.


USER
----

- Model para o controle de usuários do site. *(Feito com gem Devise)*

**MÉTODOS**
-*NEW(SESSION):* Faz o login do usuário.
-*NEW(REGISTRATION):* Faz o cadastro do usuário.
-*EDIT(REGISTRATION):* Faz a edição do cadastro do usuário.
-*NEW(PASSWORD):* Faz o processo de obter nova senha para o usuário.
-*EDIT(PASSWORD):* Faz o processo de mudança de senha.
-*NEW(SESSION):* Faz o login do usuário.
-*NEW(SESSION):* Faz o login do usuário.


UTILIZANDO O SITE
=================

- O site varia suas funcionalidades de acordo com o tipo de usuário. Por isso dipõe de botões para o login e o cadastro dos mesmos. 

USUÁRIOS NÃO CADASTRADOS
------------------------

- Usuários não cadastrados têm acesso à home page do site, onde podem ler sobre a equipe desenvolvedora, e alguns links, internos e externos. Podem também fazer seu cadastro, de forma a ter acesso às demais áreas.

USUÁRIOS CADASTRADOS (COMUNS)
-----------------------------

- Usuários comuns tem direito a editar seu cadastro, bem como excluir sua conta. Através da "Página para Usuários" podem ter acesso à alguns links, dentre eles o de Criação de Treinador Pokémon (avatar). Na aba Treinadores podem visualizar (apenas) outros treinadores criados, inclusive de outros Usuários.


-**CRIANDO UM TREINADOR POKÉMON:** Após clicar no botão "Criar Treinador", o usuário deverá preencher o formulário de criação de treinadores, preenchendo todos os campos e escolhendo seu pokémon inicial. Após isso deve-se clicar no botão "Criar Pokémon Trainer"

-**EDITANDO O TREINADOR POKÉMON:** Logo após criar o treinador o usuário normal pode atualizá-lo caso tenha ocorrigo algum erro na criação. Basta clicar no campo "Editar", alterar os campos desejados e por fim clicar em "Atualizar Pokémon Trainer".


USUÁRIOS CADASTRADOS (ADMINS)
-----------------------------

- Os usuários Administradores possuem todas as permissões dos usuários normais, porém com duas features extras. Os Admin podem ter acesso à aba exclusiva "Usuários" onde podem ver a relação de todos os usuários cadastrados, bem como sua categoria. Além disse os Admin podem, na aba "Treinadores", ver, editar e apagar qualquer dado de quelquer treinador.